const { Client } = require('pg')
const client = new Client('postgres://postgres:postgres@localhost:5432/postgres')

await client.connect()
await client.query('CREATE TABLE test(name text)')
await client.query('INSERT INTO test values (\'Hello World\')')

const res = await client.query('SELECT * FROM test')
console.log(res.rows)

await client.end()